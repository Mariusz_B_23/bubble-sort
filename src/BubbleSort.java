import java.util.Arrays;
import java.util.Random;


public class BubbleSort {

    private static int[] createArrayWithRandomSizeAndRandomNumbers() {
        Random random = new Random();
        int size = random.nextInt(30);
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(500);
        }
        return array;
    }


    static void bubbleSort(int[] array) {
        int temp;
        boolean swapped;
        int counter = 0;
        for (int i = 0; i < array.length - 1; i++) {
            counter = i++;
            swapped = false;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
        }
        System.out.println(Arrays.toString(array));
        System.out.println("Counter: " + counter);
    }

    public static void main(String[] args) {

        System.out.print("Array1:");
        int[] array1 = createArrayWithRandomSizeAndRandomNumbers();
        System.out.println();
        System.out.println(Arrays.toString(array1));
        System.out.println("Array 2 - sorted array");
        int[] array2 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23};
        System.out.println(Arrays.toString(array2));
        System.out.println();


        System.out.println("Array 1");
        bubbleSort(array1);
        System.out.println("Array 2");
        bubbleSort(array2);

    }
}
